syntax on
set tabstop=4
set shiftwidth=4
set expandtab
filetype indent on
set nu
autocmd FileType html source ~/.vim/html.vim
